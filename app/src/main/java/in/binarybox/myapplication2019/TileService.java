package in.binarybox.myapplication2019;

import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.service.quicksettings.Tile;

import androidx.annotation.RequiresApi;

@RequiresApi(api = Build.VERSION_CODES.N)
public class TileService extends android.service.quicksettings.TileService {
    private static final String TAG = "jithu";


    public TileService() {
        super();
//        Log.i(TAG,"constructor");
    }

    @Override
    public void onDestroy() {
//        Log.i(TAG,"onDestroy");
        super.onDestroy();
    }

    @Override
    public void onTileAdded() {
//        Log.i(TAG,"onTileAdded");

        updateTileDetailsFn();


        super.onTileAdded();
    }

    private void updateTileDetailsFn() {

        Tile tile = getQsTile();
        tile.setLabel("myTile");
        tile.updateTile();
    }

    @Override
    public void onTileRemoved() {
//        Log.i(TAG,"onTileRemoved");
        super.onTileRemoved();
    }

    @Override
    public void onStartListening() {
//        Log.i(TAG,"onStartListening");
        updateTileDetailsFn();
        super.onStartListening();
    }

    @Override
    public void onStopListening() {
//        Log.i(TAG,"onStopListening");
        super.onStopListening();
    }

    @Override
    public void onClick() {
        super.onClick();


    }

    @Override
    public IBinder onBind(Intent intent) {
//        Log.i(TAG,"onBind");
        return super.onBind(intent);
    }

}
